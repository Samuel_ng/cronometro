import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public class CronometroGui extends JFrame {
	
	private int centecimas;
	private int segundos;
	private int minutos;
	
	private JLabel Etiquetatiempo;
	private Timer timer;
	private JButton iniciar,detener,pausar;
	
	public CronometroGui() {
		centecimas=0;
		segundos=0;
		minutos=0;
		
		this.setSize(400,300);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(new FlowLayout());
		
		agregarBotonesyEtiqueta();
		
	}
	
	

	private void agregarBotonesyEtiqueta() {
		OyenteTimer oyenteT = new OyenteTimer();
		timer = new Timer(100,oyenteT);
		
		
		this.Etiquetatiempo= new JLabel("00:00:00");
		this.iniciar= new JButton("Iniciar");
		OyenteIniciar Inicia= new OyenteIniciar();
		iniciar.addActionListener(Inicia);
		
		this.detener= new JButton("Detener");
		OyenteDetener Odetener= new OyenteDetener();
		this.detener.addActionListener(Odetener);
		
		this.pausar= new JButton("Pausar");
		OyentePausa pausa= new OyentePausa();
		this.pausar.addActionListener(pausa);
		
		
		this.getContentPane().add(Etiquetatiempo);
		this.getContentPane().add(iniciar);
		this.getContentPane().add(detener);
		this.getContentPane().add(pausar);
		
	}



	public int getCentecimas() {
		return centecimas;
	}

	public void setCentecimas(int centecimas) {
		this.centecimas = centecimas;
	}

	public int getSegundos() {
		return segundos;
	}

	public void setSegundos(int segundos) {
		this.segundos = segundos;
	}

	public int getMinutos() {
		return minutos;
	}

	public void setMinutos(int minutos) {
		this.minutos = minutos;
	}
	
	public class OyentePausa implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			timer.stop();
			iniciar.setEnabled(true);
			iniciar.setText("Reanudar");
		}
		
	}
	
	public class OyenteDetener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			timer.stop();
			Etiquetatiempo.setText("00:00:00");
			centecimas=0; segundos=0; minutos=0;
			iniciar.setEnabled(true);
			iniciar.setText("Iniciar");
		}
		
	}
	
	public class OyenteIniciar implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			timer.start();
			iniciar.setEnabled(false);
		}

		
	}
	
	
	public class OyenteTimer implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			centecimas++;
			if(centecimas==10) {
				segundos++;
				centecimas=0;
			}
			if(segundos==60) {
				minutos++;
				segundos=0;
			}
			
			actualizarEtTiempo();
			
		}

		private void actualizarEtTiempo() {
			String tiempo="0"+minutos+":"+segundos+":0"+centecimas;
			if(segundos<10) {
				tiempo="0"+minutos+":0"+segundos+":0"+centecimas;
				
			}
			
			Etiquetatiempo.setText(tiempo);
			
		}

		
	}
	
	
}
